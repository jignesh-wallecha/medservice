const path = require('path');
const postcssPlugins = [
	require('postcss-import'),
	require('postcss-simple-vars'),
	require('postcss-nested'),
	require('autoprefixer')
];

module.exports = {
	entry: './app/assets/scripts/app.js', //entry point for webpack to see js
	output: {
		filename: 'app.bundled.js',
		path: path.resolve(__dirname, 'app')
		//in this webpack will create automatically output file. 
	},
	mode: 'development', //this mode is in development
	watch: true,  // webpack is continuosly watching the files 

	//external modules
	module: {
		rules: [
			{
				test: /\.css$/i, 
				//ki file ka nam end mai .css hona chaiye in case-insensitive
				use: [
						'style-loader',
						'css-loader?url=false',
						{
							loader: 'postcss-loader',
							options: {
								postcssOptions: {
									plugins: postcssPlugins
								}
							}
						}
					],
			},
		],
	}
}