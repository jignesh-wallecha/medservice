const path = require('path');

const postCSSPlugins = [
    require('postcss-import'),
    require('postcss-simple-vars'),
    require('postcss-nested'),
    require('autoprefixer')
]
// hot module replacement
// hot iskeliye kyuki vo runtime pe inject karega ....
module.exports = {
    entry : './app/assets/js/app.js',
    output : {
        filename : 'app.bundled.js',
        path : path.resolve(__dirname, 'app') // this method will give me the curr dir + app
    },
    devServer: {
        before : function(app,server){
            server._watch('./app/**/*.html');
        },
        contentBase : path.resolve(__dirname, 'app'),
        port : 3000,
        hot : true ,// 'hot module' feature allows injecting css and js without refreshing
        // it automatically keeps watch so no need to write watch : true
        host : '0.0.0.0'
        // open : true
    },
    
    mode : 'development', // the default mode is this only but it is good to specify!!
//    watch : true,
    module :{
            rules : [
            {
                test : /\.css$/i, // in js regex is written between '/regex-comes-here/'
                use : [
                        'style-loader', 
                        'css-loader?url=false',
                        {
                            loader : 'postcss-loader',
                            options : {
                                postcssOptions: {
                                    plugins : postCSSPlugins
                                }
                            }
                        }
                      ],
                // 'url = false is written to tell the loader that i will handle the background : url('') manually'
            },
        ],
    }
}